﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PushDAL
{
    public class WeightDTO
    {
        public int UserID { get; set; }
        public double WeightAmount { get; set; }
        public DateTime WeightDate { get; set; }

        public WeightDTO(int userid)
        {
            this.UserID = userid;
        }

        public WeightDTO(int userid, double weightAmount, DateTime weightDate)
        {
            this.UserID = userid;
            this.WeightAmount = weightAmount;
            this.WeightDate = weightDate;
        }

        public WeightDTO(double weightAmount, DateTime weightDate)
        {
            this.WeightAmount = weightAmount;
            this.WeightDate = weightDate;
        }
    }
}
