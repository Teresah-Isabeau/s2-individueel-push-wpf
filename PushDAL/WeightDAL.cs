﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace PushDAL
{
    public class WeightDAL : IWeight, IWeightContainer
    {
        private string connectionString = @"Server = mssql.fhict.local; Database = dbi459672; User Id = dbi459672; Password = Pusheen2021!;";

        public SqlConnection sqlConnection;

        public WeightDAL()
        {
            sqlConnection = new SqlConnection(connectionString);
        }

        //Opent de connectie
        private void Open()
        {
            try
            {
                sqlConnection.Open();
            }
            catch (Exception er)
            {
                MessageBox.Show("Connection Error ! " + er.Message, "Information");
            }
        }

        //Sluit de connectie
        private void Close()
        {
            sqlConnection.Close();
        }

        public int AddWeight(WeightDTO weightDTO)
        {
            Open();
            SqlCommand command = new SqlCommand("dbo.AddNewWeight", this.sqlConnection);
            command.CommandType = CommandType.StoredProcedure;
            try
            {
                command.Parameters.AddWithValue("@UserID", weightDTO.UserID);
                command.Parameters.AddWithValue("@Weight", weightDTO.WeightAmount);
                command.Parameters.AddWithValue("@WeightDate", weightDTO.WeightDate);
                return command.ExecuteNonQuery();
            }
            catch
            {
                throw;
            }
            finally
            {
                command.Dispose();
                Close();
            }
        }

        //haal de gewichten op van 1 gebruiker
        public List<WeightDTO> GetAllWeight(int userID)
        {
            Open();
            SqlCommand command = new SqlCommand("dbo.GetAllWeightFromAnUser", sqlConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@UserID", userID);
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            DataTable table = new DataTable();
            adapter.Fill(table);          
            command.Dispose();
            Close();

            List<WeightDTO> weights = new List<WeightDTO>();

            foreach (DataRow row in table.Rows)
            {
                WeightDTO weight = new WeightDTO(
                    Convert.ToInt32(row["UserID"]),
                    Convert.ToDouble(row["Weight"]),
                    Convert.ToDateTime(row["WeightDate"])
                    );
                weights.Add(weight);
            }
            return weights;
        }
    }
}
