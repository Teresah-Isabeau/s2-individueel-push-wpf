﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace PushDAL
{
    public class UserDAL : IUser, IUserContainer
    {
        private string connectionString = @"Server = mssql.fhict.local; Database = dbi459672; User Id = dbi459672; Password = Pusheen2021!;";

        public SqlConnection sqlConnection;

        public UserDAL()
        {
            sqlConnection = new SqlConnection(connectionString);
        }

        //Opent de connectie
        private void Open()
        {
            try
            {
                sqlConnection.Open();
            }
            catch (Exception er)
            {
                MessageBox.Show("Connection Error ! " + er.Message, "Information");
            }
        }

        //Sluit de connectie
        private void Close()
        {
            sqlConnection.Close();
        }

        //insert

        public int Insert(UserDTO userDTO)
        {
            Open();
            SqlCommand command = new SqlCommand("dbo.CreateNewUser", this.sqlConnection);
            command.CommandType = CommandType.StoredProcedure;
            try
            {
                command.Parameters.AddWithValue("@Email", userDTO.Email);
                command.Parameters.AddWithValue("@Password", userDTO.Password);
                command.Parameters.AddWithValue("@Name", userDTO.Name);
                command.Parameters.AddWithValue("@Length", userDTO.Length);
                return command.ExecuteNonQuery();
            }
            catch
            {
                throw;
            }
            finally
            {
                command.Dispose();
                Close();
            }
        }

        //Checkt of de gebruiker bestaat in de database
        public bool CheckUserExists(UserDTO userDTO)
        {
            Open();
            SqlCommand command = new SqlCommand("dbo.CheckUserExists", sqlConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@Email", userDTO.Email);
            command.Parameters.AddWithValue("@Password", userDTO.Password);
            SqlDataReader reader = command.ExecuteReader();
            try
            {
                return reader.HasRows;
                //if (reader.HasRows)
                //{
                //    reader.Read();
                //    return true;
                //}
                //else
                //{
                //    return false;
                //}
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                Close();
            }
        }

        //Checkt of ingevulde al bestaat in de database
        public bool CheckUniqueEmail(UserDTO userDTO)
        {
            Open();
            SqlCommand command = new SqlCommand("dbo.CheckUniqueEmail", sqlConnection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@Email", userDTO.Email);
            SqlDataReader reader = command.ExecuteReader();
            try
            {
                return reader.HasRows;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Dispose();
                Close();
            }
        }


        //update
        //delete
    }
}
