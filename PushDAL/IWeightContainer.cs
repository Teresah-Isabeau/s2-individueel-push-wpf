﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PushDAL
{
    public interface IWeightContainer
    {
        List<WeightDTO> GetAllWeight(int userID);
    }
}
