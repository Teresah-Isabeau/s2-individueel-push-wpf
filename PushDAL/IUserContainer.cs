﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PushDAL
{
    public interface IUserContainer
    {
        bool CheckUserExists(UserDTO userDTO);
        bool CheckUniqueEmail(UserDTO userDTO);
    }
}
