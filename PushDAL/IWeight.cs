﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PushDAL
{
    public interface IWeight
    {
        int AddWeight(WeightDTO weightDTO);
    }
}
