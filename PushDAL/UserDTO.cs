﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PushDAL
{
    public class UserDTO
    {
        public int UserID { get; set; }
        public string Name { get; private set; }
        public string Email { get; private set; }
        public string Password { get; private set; }
        public int Length { get; private set; }

        public UserDTO(string email)
        {
            this.Email = email;
        }

        public UserDTO(int userID)
        {
            this.UserID = userID;
        }

        public UserDTO(string email, string password)
        {
            this.Email = email;
            this.Password = password;
        }

        public UserDTO(string name, string email, string password, int length)
        {
            this.Name = name;
            this.Email = email;
            this.Password = password;
            this.Length = length;
        }
    }
}

