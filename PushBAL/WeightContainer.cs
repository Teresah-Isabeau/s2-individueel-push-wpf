﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PushDAL;

namespace PushBAL
{
    public class WeightContainer
    {
        readonly WeightDAL weightDAL = new WeightDAL();
        private IWeightContainer IWeightContainer;

        public WeightContainer(IWeightContainer IweightContainer)
        {
            this.IWeightContainer = IweightContainer;
        }

        public WeightDTO WeightToWeightDTO(Weight weight)
        {

            return new WeightDTO(weight.WeightAmount, weight.WeightDate);
        }

        public List<Weight> GetAllWeight(int userID)
        {
            List<WeightDTO> weightsDTO = weightDAL.GetAllWeight(userID);
            List<Weight> weightList = new List<Weight>();

            foreach (WeightDTO weightDTO in weightsDTO)
            {                           
                weightList.Add(new Weight(weightDTO.WeightAmount, weightDTO.WeightDate));
            }
            return weightList;
        }
    }
}
