﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PushDAL;

namespace PushBAL
{
    public class Weight
    {
        private IWeight Iweight;

        public Weight(IWeight weight)
        {
            this.Iweight = weight;
        }

        public int UserID { get; private set; }
        public double WeightAmount { get; private set; }
        public DateTime WeightDate { get; private set; }

        public Weight(int userid, double weightAmount, DateTime weightDate)
        {
            this.UserID = userid;
            this.WeightAmount = weightAmount;
            this.WeightDate = weightDate;
        }

        public Weight(double weightAmount, DateTime weightDate)
        {
            this.WeightAmount = weightAmount;
            this.WeightDate = weightDate;
        }

        public WeightDTO WeightToWeightDTO(Weight weight)
        {

            return new WeightDTO(weight.UserID ,weight.WeightAmount, weight.WeightDate);
        }

        public int AddWeight(IWeight iWeight)
        {
            return iWeight.AddWeight(WeightToWeightDTO(this));
        }
    }
}
