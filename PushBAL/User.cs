﻿using PushDAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PushBAL
{
    public class User
    {
        private IUser Iuser;

        public User(IUser user)
        {
            this.Iuser = user;
        }

        public int UserID { get; set; }
        public string Name { get; private set; }
        public string Email { get; private set; }
        public string Password { get; private set; }
        public int Length { get; private set; }


        public User(string email)
        {
            this.Email = email;
        }

        public User(int userID)
        {
            this.UserID = userID;
        }

        public User(string email, string password)
        {
            this.Email = email;
            this.Password = password;
        }

        public User(string name, string email, string password, int length)
        {
            this.Name = name;
            this.Email = email;
            this.Password = password;
            this.Length = length;
        }

        public UserDTO UserToUserDTO(User user)
        {
            return new UserDTO(user.Name, user.Email, user.Password, user.Length);
        }

        public int Insert(IUser iUser)
        {
            return iUser.Insert(UserToUserDTO(this));
        }
    }
    
}
