﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PushDAL;

namespace PushBAL
{
    public class UserContainer
    {
        private IUserContainer IUserContainer;

        public UserContainer(IUserContainer userContainer)
        {
            this.IUserContainer = userContainer;
        }

        public UserDTO UserToUserDTO(User user)
        {
            return new UserDTO(user.Name, user.Email, user.Password, user.Length);
        }

        public bool CheckUserExists(User user)
        {
            return IUserContainer.CheckUserExists(UserToUserDTO(user));
        }

        public bool CheckUniqueEmail(User user)
        {
            return IUserContainer.CheckUniqueEmail(UserToUserDTO(user));
        }
    }
}
