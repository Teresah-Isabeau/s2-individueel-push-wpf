﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System;
using PushBAL;
using PushDAL;
using System.Data;

namespace PushUnitTest
{
    [TestClass]
    public class PushUnitTest
    {
        [TestMethod]
        public void TestUserExists()
        {
            // Arrange
            //User user = new User("test@live.nl", "TEST");
            //UserDAL userDAL = new UserDAL();
            //UserContainer userContainer = new UserContainer(userDAL);
            
            //// Act
            //DataTable dt = userContainer.CheckUserExists(user);

            //// Assert 
            //Assert.AreEqual(1, dt.Rows.Count);
        }



        [TestMethod]
        public void TestGetAllWeight()
        {
            // Arrange
            WeightContainer weightContainer = new WeightContainer(new WeightDAL());

            // Act
            int userID = 2;
            IList<Weight> weights = weightContainer.GetAllWeight(userID);

            // Assert
            Assert.AreEqual(1, weights.Count);
        }
    }
}
