﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using PushBAL;
using PushDAL;


namespace Push
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : Window
    {
        User user = new User(new UserDAL());
        UserContainer userContainer = new UserContainer(new UserDAL());

        DataTable dt; 

        public Login()
        {
            InitializeComponent();
            dt = new DataTable();
        }

        private void BtnLogin_Click(object sender, RoutedEventArgs e)
        {
            LoginCheck();     
        }

        private void BtnCreateAccount_Click(object sender, RoutedEventArgs e)
        {
            gbCreateAccount.Visibility = Visibility.Visible;
        }

        private void BtnCreateNewAccount_Click(object sender, RoutedEventArgs e)
        {
            CheckEmail();
            AddNewAccount();
            MessageBox.Show("Created new account!");
            gbCreateAccount.Visibility = Visibility.Collapsed;
        }

        /// <summary>
        /// Checkt of de email en password overeen komen in de database
        /// </summary>
        private void LoginCheck()
        {
            
            if (tbEmail.Text == "")
            {
                MessageBox.Show("Enter Username", "Error");
            }
            if (tbPassword.Text == "")
            {
                MessageBox.Show("Enter Password", "Error");
            }
            else
            {
                User user = new User
                (
                    tbEmail.Text,
                    tbPassword.Text
                );
                
                //DataTable dt= userContainer.CheckUserExists(user);
                if (userContainer.CheckUserExists(user) == true)
                {
                    //user = userContainer.FetchUser(user);
                    //user.UserID = dt.Rows[0].Field<int>("UserID");
                    MainWindow mainWindow = new MainWindow();
                    mainWindow.SetUser(user);
                    mainWindow.Show();
                    this.Hide();
                }
                else
                {
                    MessageBox.Show("onjuist");
                    tbEmail.Focus();
                }
            }
        }

        /// <summary>
        /// Maakt een nieuw account aan in de Database
        /// </summary>
        private void AddNewAccount()
        {
            if ((tbNewName.Text == "") || (tbNewEmail.Text == "") || (tbNewPassword.Text == "") || (tbNewWeight.Text == "") || (tbNewLength.Text == ""))
            {
                MessageBox.Show("Fill a fields");
            }
            if (CheckEmail() == true)
            {
            }
            else
            {
                int intResult = 0;
                User user = new User(tbNewName.Text, tbNewEmail.Text, tbNewPassword.Text, Convert.ToInt32(tbNewLength.Text));
                
                intResult = user.Insert(new UserDAL()); 

                if (intResult > 0)
                {
                    MessageBox.Show("New record inserted successfully.");
                }
                
                MainWindow mainWindow = new MainWindow();
                mainWindow.Show();
                this.Hide();
            }
        }

        /// <summary>
        /// checkt of de email al eerder is aangemaakt in de database
        /// deze moet uniek zijn
        /// </summary>
        /// <returns></returns>
        private bool CheckEmail()
        {
            User user = new User(tbNewEmail.Text);
            //DataTable table = userContainer.CheckUniqueEmail(user);
            if (userContainer.CheckUniqueEmail(user))
            {
                MessageBox.Show("Email exist");
                tbEmail.Clear();
                return true;
            }
            return false;
        }

        /// <summary>
        /// checkt of bij tbEmail een email adress is ingevuld door te kijken naar '@' en '.'
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void tbEmail_Leave(object sender, EventArgs e)
        {
            if (!this.tbEmail.Text.Contains('@') || !this.tbEmail.Text.Contains('.'))
            {
                MessageBox.Show("Please Enter A Valid Email");
                tbEmail.Focus();
            }
        }
    }
}
