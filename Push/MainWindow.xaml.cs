﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.VisualBasic;
using Microsoft.Win32;
using System.Data.SqlClient;
using PushBAL;
using PushDAL;

namespace Push
{
    public partial class MainWindow : Window
    {
        
        WeightContainer weightContainer = new WeightContainer(new WeightDAL());
        User User;
        public MainWindow()
        {
            InitializeComponent();
            //lbStartWeight.Content = GetStartWeight();
            //lbCurrentWeight.Content = GetLastWeight();
        }

        /// <summary>
        /// Set een user zodat het programma die ID kent
        /// </summary>
        /// <param name="user"></param>
        public void SetUser(User user)
        {
            this.User = user;
        }

        private void EnterWeight_Click(object sender, RoutedEventArgs e)
        {
            InputBox.Visibility = Visibility.Visible; 
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            AddWeight();
            InputBox.Visibility = Visibility.Collapsed;
            tbNewWeight.Text = String.Empty;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {         
            InputBox.Visibility = Visibility.Collapsed;
            tbNewWeight.Text = String.Empty;
        }

             
        private void btnShowAllWeight_Click(object sender, RoutedEventArgs e)
        {
            GBAllWeight.Visibility = Visibility.Visible;
            ShowAllWeight();
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            GBAllWeight.Visibility = Visibility.Collapsed;
        }

        /// <summary>
        /// Voegt het ingevoerde gewicht toe aan weightcontainer in de BAL die het door leid van WeightDTO > WeightDAL > Database
        /// </summary>
        private void AddWeight()
        {
            if ((tbNewWeight.Text == ""))
            {
                MessageBox.Show("Fill a fields");
            }
            else
            {
                Weight weight = new Weight(
                    this.User.UserID,
                    Convert.ToDouble(tbNewWeight.Text),
                    DateTime.Now);
                weight.AddWeight(new WeightDAL());
            }
        }

        private void ShowAllWeight()
        {          
            List<Weight> weights = weightContainer.GetAllWeight(this.User.UserID);
            this.DGAllWeight.ItemsSource = weights;
        }

        private double GetStartWeight()
        {
            List<Weight> weights = weightContainer.GetAllWeight(this.User.UserID);
            double firstWeight = Convert.ToDouble(weights.First());

            return firstWeight;
        }

        private double GetLastWeight()
        {
            List<Weight> weights = weightContainer.GetAllWeight(this.User.UserID);
            double lastWeight = Convert.ToDouble(weights.Last());

            return lastWeight;
        }
    }
}
